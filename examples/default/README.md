# Default example

## Usage

```
# terraform init
# terraform apply
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.15 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.15 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_advanced"></a> [advanced](#module\_advanced) | ../../ | n/a |
| <a name="module_default"></a> [default](#module\_default) | ../../ | n/a |

## Resources

| Name | Type |
|------|------|
| [random_string.advanced](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [random_string.default](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [aws_caller_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.example](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_access_key"></a> [aws\_access\_key](#input\_aws\_access\_key) | n/a | `string` | `null` | no |
| <a name="input_aws_assume_role"></a> [aws\_assume\_role](#input\_aws\_assume\_role) | n/a | `string` | `null` | no |
| <a name="input_aws_profile"></a> [aws\_profile](#input\_aws\_profile) | n/a | `string` | `null` | no |
| <a name="input_aws_secret_key"></a> [aws\_secret\_key](#input\_aws\_secret\_key) | n/a | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_advanced_iam_group_arn"></a> [advanced\_iam\_group\_arn](#output\_advanced\_iam\_group\_arn) | n/a |
| <a name="output_advanced_iam_group_id"></a> [advanced\_iam\_group\_id](#output\_advanced\_iam\_group\_id) | n/a |
| <a name="output_advanced_iam_group_name"></a> [advanced\_iam\_group\_name](#output\_advanced\_iam\_group\_name) | n/a |
| <a name="output_advanced_iam_group_unique_id"></a> [advanced\_iam\_group\_unique\_id](#output\_advanced\_iam\_group\_unique\_id) | n/a |
| <a name="output_advanced_iam_policy_arn"></a> [advanced\_iam\_policy\_arn](#output\_advanced\_iam\_policy\_arn) | n/a |
| <a name="output_advanced_iam_policy_id"></a> [advanced\_iam\_policy\_id](#output\_advanced\_iam\_policy\_id) | n/a |
| <a name="output_advanced_iam_policy_path"></a> [advanced\_iam\_policy\_path](#output\_advanced\_iam\_policy\_path) | n/a |
| <a name="output_advanced_iam_policy_policy_id"></a> [advanced\_iam\_policy\_policy\_id](#output\_advanced\_iam\_policy\_policy\_id) | n/a |
| <a name="output_advanced_iam_role_arn"></a> [advanced\_iam\_role\_arn](#output\_advanced\_iam\_role\_arn) | n/a |
| <a name="output_advanced_iam_role_id"></a> [advanced\_iam\_role\_id](#output\_advanced\_iam\_role\_id) | n/a |
| <a name="output_advanced_iam_role_name"></a> [advanced\_iam\_role\_name](#output\_advanced\_iam\_role\_name) | n/a |
| <a name="output_advanced_iam_role_unique_id"></a> [advanced\_iam\_role\_unique\_id](#output\_advanced\_iam\_role\_unique\_id) | n/a |
| <a name="output_iam_group_arn"></a> [iam\_group\_arn](#output\_iam\_group\_arn) | n/a |
| <a name="output_iam_group_id"></a> [iam\_group\_id](#output\_iam\_group\_id) | n/a |
| <a name="output_iam_group_name"></a> [iam\_group\_name](#output\_iam\_group\_name) | n/a |
| <a name="output_iam_group_unique_id"></a> [iam\_group\_unique\_id](#output\_iam\_group\_unique\_id) | n/a |
| <a name="output_iam_policy_arn"></a> [iam\_policy\_arn](#output\_iam\_policy\_arn) | n/a |
| <a name="output_iam_policy_id"></a> [iam\_policy\_id](#output\_iam\_policy\_id) | n/a |
| <a name="output_iam_policy_path"></a> [iam\_policy\_path](#output\_iam\_policy\_path) | n/a |
| <a name="output_iam_policy_policy_id"></a> [iam\_policy\_policy\_id](#output\_iam\_policy\_policy\_id) | n/a |
| <a name="output_iam_role_arn"></a> [iam\_role\_arn](#output\_iam\_role\_arn) | n/a |
| <a name="output_iam_role_id"></a> [iam\_role\_id](#output\_iam\_role\_id) | n/a |
| <a name="output_iam_role_name"></a> [iam\_role\_name](#output\_iam\_role\_name) | n/a |
| <a name="output_iam_role_unique_id"></a> [iam\_role\_unique\_id](#output\_iam\_role\_unique\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

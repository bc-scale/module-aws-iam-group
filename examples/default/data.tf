data "aws_caller_identity" "this" {}

data "aws_iam_policy_document" "example" {
  statement {
    effect = "Allow"
    actions = [
      "ec2:*",
    ]
    resources = ["*"]
  }
}

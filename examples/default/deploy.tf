#####
# Default
#####

resource "random_string" "default" {
  length = 8

  special = false
}

module "default" {
  source = "../../"

  prefix = random_string.default.result
}

#####
# Advanced
# - use account_ids
# - restrict to current organization
# - attach supplementary roles
# - add additional document to the policy
# - attach external policies to created group and role
#####

resource "random_string" "advanced" {
  length = 8

  special = false
}

module "advanced" {
  source = "../../"

  prefix = random_string.advanced.result

  account_ids = [
    data.aws_caller_identity.this.account_id,
  ]
  restrict_to_current_organization = true

  iam_group_name = "test"
  iam_group_policy_arns = {
    0 = "arn:aws:iam::aws:policy/ReadOnlyAccess"
  }
  iam_policy_name = "test"
  iam_policy_additional_allowed_switch_roles = [
    "arn:aws:iam:::role/actions/EC2ActionsAccess",
  ]
  iam_policy_additional_document = data.aws_iam_policy_document.example.json
  iam_role_name                  = "test"
  iam_role_policy_arns = {
    0 = "arn:aws:iam::aws:policy/ReadOnlyAccess"
  }

  tags = {
    "cause" = "Terraform test"
  }
}
